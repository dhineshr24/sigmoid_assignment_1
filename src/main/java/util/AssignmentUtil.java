package util;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AssignmentUtil {

    public static int noOfFilesInDirectory(String path) {
        List<String> fileNames = new ArrayList<>();
        try {
            DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(path));
            for (Path eachPath : directoryStream) {
                fileNames.add(eachPath.toString());
            }
        } catch (IOException ignored) {
        }
        return fileNames.size();
    }
}
